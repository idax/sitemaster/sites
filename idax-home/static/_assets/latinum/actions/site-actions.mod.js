import latinum     from "/shared/latinum/latinum.mod.js";
import welcomeAxns from "./welcome-actions.mod.js";

latinum.registerAction("welcomeAxns", welcomeAxns);