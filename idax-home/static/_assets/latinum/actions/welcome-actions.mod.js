import Logger            from "/shared/logger/shared-logger.mod.js";
import ShortcutPopulator from "../../oscar/shortcut-populator.mod.js";
import XsBlogPopulator   from "../../proton/xs-blog-populator.mod.js";

var _L = Logger.get("welcome");

var welcomeAxns = new function() {

	var monologPop = new XsBlogPopulator("#monolog");
	monologPop.setDataLocation("/monolog/blog/index.xml");
	monologPop.setCount(2);

	var vadePop = new XsBlogPopulator("#vademecum");
	vadePop.setDataLocation("/potteringeek/blog/index.xml");
	vadePop.setCount(2);

	var sideProj = new ShortcutPopulator("#sideprojects", "side-projects");
	sideProj.setCount(3);

	this.loadPage = function() {
		monologPop.setup();
		vadePop.setup();
		sideProj.setup();
	}

	this.unloadPage = function() {
		monologPop.teardown();
		vadePop.teardown();
		sideProj.teardown();
	}
}

export default welcomeAxns;